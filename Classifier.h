#ifndef CLASSIFIER_H_INCLUDED
#define CLASSIFIER_H_INCLUDED

#include "opencv2/core/core.hpp"
#include "opencv2/ml/ml.hpp"

#include <chrono>
#include <cstdio>
#include <vector>
#include <iostream>
#include <ctime>

using namespace std;
using namespace cv;
using namespace cv::ml;
using namespace chrono;

namespace dip
{

class Classifier
{
    Ptr<ANN_MLP>        _model;
    FeatureGenerator    _featureGen;
    double              _fps;
    bool                _isImage;

    /** for debugging */
    const char*    NAME = "[Classifier]";
    const char*    SRC_WINDOW = "FeatureSource";

    bool read_num_class_data( const string& filename, int var_count,
                         Mat* _data, Mat* _responses )
    {
        const int M = 1024;
        char buf[M+2];

        Mat el_ptr(1, var_count, CV_32F);
        int i;

        vector<int> responses;

        _data->release();
        _responses->release();

        FILE* f = fopen( filename.c_str(), "rt" );
        if( !f )
        {
            cout << "Could not read the database " << filename << endl;
            return false;
        }

        for(;;)
        {
            char* ptr;
            if( !fgets( buf, M, f ) || !strchr( buf, ',' ) )
                break;
            responses.push_back(buf[0]);
            cout << "responses " << buf[0] << " " ;;//<<  endl;
            ptr = buf+2;
            for( i = 0; i < var_count; i++ )
            {
                int n = 0;
                sscanf( ptr, "%f%n", &el_ptr.at<float>(i), &n );
                ptr += n + 1;
            }
            cout << el_ptr << endl;

            if( i < var_count )
                break;
            _data->push_back(el_ptr);
        }
        fclose(f);
        Mat(responses).copyTo(*_responses);

        cout << "The database " << filename << " is loaded.\n";

        return true;
    }

    template<typename T>
    Ptr<T> load_classifier(const string& filename_to_load)
    {
        // load classifier from the specified file
        Ptr<T> model = StatModel::load<T>( filename_to_load );
        if( model.empty() )
            cout << "Could not read the classifier " << filename_to_load << endl;
        else
            cout << "The classifier " << filename_to_load << " is loaded.\n";

        return model;
    }

public:
    Classifier(){
        namedWindow( SRC_WINDOW, WINDOW_AUTOSIZE );
        _fps = 0.0f;
        _isImage = false;
    };

    ~Classifier(){};

    bool Build(const string& data_filename, const string& filename_to_load )
    {
        Mat data;
        Mat responses;

        bool ok = read_num_class_data( data_filename, 20, &data, &responses );///third parameter: FEATURES
        if( !ok )
            return ok;

        int nsamples_all = data.rows;
        int ntrain_samples = (int)(nsamples_all*1.0);//SPLIT

        /// Create or load MLP classifier
        if( !filename_to_load.empty() )
        {
            _model = load_classifier<ANN_MLP>(filename_to_load);
            if( _model.empty() )
                return false;
        }

        int training_correct_predict=0;

        /// compute prediction error on training data
        for(int i = 0; i < nsamples_all; i++ )
        {
            Mat sample = data.row(i);
            /// cout << "Sample: " << responses.at<int>(i)-48 << " row " << data.row(i) << endl;
            float r = _model->predict( sample );
            /// cout << "Predict:  r = " << r << endl;

            if( (int)r == (int)(responses.at<int>(i)-48) ) //prediction is correct
                training_correct_predict++;
        }

        printf("ntrain_samples %d training_correct_predict %d \n",ntrain_samples, training_correct_predict);
        printf( "\nTest Recognition rate: training set = %.1f%% \n\n", training_correct_predict*100.0/ntrain_samples);

        return true;
    }

    void Predict(Mat srcColor)
    {
        vector< float > descriptors;
        system_clock::time_point start = system_clock::now();

        _featureGen.SetSource(srcColor);
        _featureGen.GetFourierDescriptors(descriptors, true);
#if 1
        Mat_<float> samplef(1, 20, &descriptors[0]);
        float r = _model->predict( samplef );
        cout << NAME << "Prediction: " << r << endl;
#else
        Mat sample1 = (Mat_<float>(1,20) << 2.000000,0.456808,0.145986,0.108563,0.070925,0.042733,0.038871,0.040995,0.019335,0.028788,0.020787,0.011624,0.014388,0.026733,0.016064,0.013934,0.018600,0.015046,0.013161,0.006654);// 1
        float r = _model->predict( sample1 );    /// 1
        cout << "Prediction: " << r << " Answer: " << 1 << endl;
        sample1 = (Mat_<float>(1,20) << 2.000000,0.772039,0.402081,0.403232,0.285556,0.429642,0.190573,0.145267,0.098396,0.051271,0.082368,0.046241,0.021241,0.046169,0.022724,0.028471,0.029202,0.033885,0.039122,0.013184);//2
        r = _model->predict( sample1 );          /// 5
        cout << "Prediction: " << r << " Answer: " << 5 << endl;
        sample1 = (Mat_<float>(1,20) << 2.000000,0.571998,0.101517,0.328579,0.273885,0.224774,0.090468,0.094689,0.035262,0.044024,0.061105,0.025589,0.028430,0.028924,0.022099,0.033110,0.013663,0.011107,0.024943,0.015537);//3
        r = _model->predict( sample1 );          /// 9
        cout << "Prediction: " << r << " Answer: " << 9 << endl;
        sample1 = (Mat_<float>(1,20) << 2.000000,0.881922,0.293449,0.221181,0.181222,0.082583,0.024944,0.067493,0.050544,0.045810,0.030091,0.021793,0.035789,0.034018,0.014260,0.026835,0.011992,0.021717,0.013773,0.008907);//5
        r = _model->predict( sample1 );          /// 2
        cout << "Prediction: " << r << " Answer: " << 2 << endl;
        sample1 = (Mat_<float>(1,20) << 2.000000,0.480928,0.492774,0.487823,0.195034,0.281336,0.139961,0.108654,0.081112,0.048706,0.063123,0.032424,0.033118,0.031453,0.027343,0.033681,0.022068,0.019182,0.021726,0.011882);//6
        r = _model->predict( sample1 );          /// 3
        cout << "Prediction: " << r << " Answer: " << 3 << endl;
        sample1 = (Mat_<float>(1,20) << 2.000000,0.294570,0.276506,0.342098,0.272842,0.151997,0.097665,0.085440,0.139085,0.050375,0.086254,0.050546,0.047491,0.035993,0.029207,0.035033,0.027308,0.025466,0.031005,0.021016);//7
        r = _model->predict( sample1 );          /// 7
        cout << "Prediction: " << r << " Answer: " << 7 << endl;
#endif
        char printit[100];
        if(_isImage)
            sprintf(printit,"Gesture: %d", (int)r);
        else
            sprintf(printit,"%2.1f, Gesture: %d", _fps, (int)r);

        putText(srcColor, printit, cvPoint(10,30), FONT_HERSHEY_PLAIN, 1.5, cvScalar(0,255,0), 2, 8);
        imshow( SRC_WINDOW, srcColor );

        system_clock::time_point end = system_clock::now();
        double seconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        _fps = 1000000/seconds;

    }

    void Predict(const char* filePath)
    {
        Mat srcColor = imread(filePath, IMREAD_COLOR);
        if (srcColor.empty())
        {
            cerr << NAME << __FUNCTION__ << "fail to file read" << endl;
            return;
        }

        _isImage = true;
        Predict(srcColor);
    }

};
}

#endif // CLASSIFIER_H_INCLUDED
