#include <iostream>
#include "FeatureGenerator.h"
#include "DatasetGenerator.h"
#include "Classifier.h"

using namespace std;
using namespace dip;

#define CLASSIFIER
///#define DATASETGEN

int main(int argc, char *argv[])
{

#ifdef DATASETGEN
    DatasetGenerator    dataSetGen;
    dataSetGen.SetSource("./img");
    dataSetGen.GenerateDataset("Features.data");
#else
    Classifier  classifier;
    classifier.Build("Features.data", "gesture.xml");

    if(argc == 2)
    {
        cout << "Recognizing Gesture in a still image" << argv[1] << endl;
        classifier.Predict(argv[1]);
    }
    else
    {
        VideoCapture cap;
        cap.open(0);
        Mat src;

        cout << "Recognizing Gestures in captured video " << endl;

        while (1)
        {
            cap >> src;
            if( src.empty() )
                break;

            char key = (char)waitKey(10);
            if(key==113 || key==27)
                break;

            classifier.Predict(src);
        }
    }
#endif // ifdef

    waitKey();
    return 0;
}
