#ifndef DATASETGENERATOR_H_INCLUDED
#define DATASETGENERATOR_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "FeatureGenerator.h"


namespace dip{

using namespace std;

class DatasetGenerator
{
    string          _dirPath;

    int getClassNum(string& fileName){
        int classNum = -1;
        size_t pos = fileName.find_first_of('_');
        if(pos + 1 <= fileName.length()) {
            classNum = fileName[pos + 1] - '0';
        }

        return classNum;
    }

    bool getFileNames(vector<string>& fileNames)
    {
        string dir, filepath;
        DIR *dp;
        struct dirent *dirp;
        struct stat filestat;

        printf("[%s] Open directory %s \n", __FUNCTION__, _dirPath.c_str());

        dp = opendir(_dirPath.c_str());
        if (dp == NULL)
        {
            printf("[%s] Fail to open directory %s \n", __FUNCTION__, _dirPath.c_str());
            return false;
        }

        while ((dirp = readdir( dp )))
        {
            filepath = _dirPath + "/" + dirp->d_name;

            /// If the file is a directory (or is in some way invalid) skip it
            if (stat( filepath.c_str(), &filestat )) continue;
            if (S_ISDIR( filestat.st_mode ))         continue;

            fileNames.push_back(dirp->d_name);
        }

        closedir( dp );

        return true;
    }

    bool writeData(const char* outFile, int classNum, vector<float>& data)
    {
        FILE* file = fopen(outFile, "a");
        if (!file) {
            printf("[%s] Could not open dataset file (%s) for appending \n", __FUNCTION__, outFile);
            return false;
        }

        printf("[%s] writeData %d features \n", __FUNCTION__, (int)data.size());

        fprintf(file, "%d,", classNum);

        for(uint32_t i = 0; i < data.size(); i++) {
            fprintf(file, "%f", data[i]);
            if(i != data.size() - 1)
                fputc(',', file);
        }

        fputc('\n', file);

        fclose(file);

        return true;
    }

public :
    DatasetGenerator() {

    }

    ~DatasetGenerator() {
    }

    bool SetSource(const char* dirPath)
    {
        _dirPath.clear();
        _dirPath.append(dirPath);
        return true;
    }

    bool GenerateDataset(const char* outFileName)
    {
        vector<string>      fileNames;
        FeatureGenerator    featureGen;
        vector<float>       features;

        if(getFileNames(fileNames) == false)
            return false;

        for(uint32_t i = 0; i < fileNames.size(); i++)
        {
            /// get classnum
            int classNum = getClassNum(fileNames[i]);

            printf("[%s] Start to get features from %s \n", __FUNCTION__, fileNames[i].c_str());

            /// get Features
            featureGen.SetSource((_dirPath + "/" + fileNames[i]).c_str());
            featureGen.GetFourierDescriptors(features, false);

            /// write dataset
            writeData(outFileName, classNum, features);

            printf("[%s] Finish to write features to %s \n", __FUNCTION__, outFileName);
        }

        return true;
    }
};


}

#endif // DATASETGENERATOR_H_INCLUDED
