# README #

### What is this repository for? ###

* Quick summary : OpenCV program to recognize human gesture with multi-layer perceptrons (MLP), the most commonly used type of neural networks 
 machine leaning algorithm.
* Version : 1.0
* [Learn more](http://docs.opencv.org/2.4/modules/ml/doc/neural_networks.html)

### Descriptions ###

This program written in C++ uses OpenCV to segments, finds the contours and compute the Fourier Descriptors for that contour. The segmentation is done by thresholding a grey-scale image to segment the hand. 
The Fourier Descriptor is used to generate training data set for machine learning. 
A classifier from the training data is able to detect 10 different gestures from a image file or from camera input in real time.

### How to use ###
* Please let you execute Code::Block project on Linux OS
* If the program takes one argument, e.g.: ./RecognisingGestures image1.png
then it processes a single static image. The program prints decoded number  corresponding gesture on the resulting image.
* If no arguments are given, then the program enters in dynamic mode and use the web-camera. The program shows the fps and the gesture classification

### Screen Capture ###

![RecognisingGesture.png](https://bitbucket.org/repo/yp8aoXd/images/2435052807-RecognisingGesture.png)

### Important Information ###

* The used algorithm utilizing the Fourier Descriptor for recognizing gesture is not my idea. It is came from an article.