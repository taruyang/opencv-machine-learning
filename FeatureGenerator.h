#ifndef FEATUREGENERATOR_H_INCLUDED
#define FEATUREGENERATOR_H_INCLUDED

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>

namespace dip {

using namespace cv;
using namespace std;

class FeatureGenerator {

    Mat                     _srcColor;
    vector< vector<Point> > _contours;
    int                     _largestContour;
    const int               NUM_CE = 20;

    /** for debugging */
    const char*    NAME = "[FeatureGenerator]";
    const char*    PROC_WINDOW = "SegmentedSource";
    const char*    OUT_WINDOW = "ContourImage";

        /// Get contour pixels
    bool extractContour(bool showProcess) {

        Mat     srcGray;
        cvtColor(_srcColor, srcGray, COLOR_BGR2GRAY);

        /// Segmentation
        ///GaussianBlur(_srcGray, _srcGray, Size(9, 9), 2, 2 );
        blur(srcGray, srcGray, Size(3,3));
        Mat outSegmented;
        threshold(srcGray, outSegmented, 0, 255, THRESH_BINARY | THRESH_OTSU);

        /// Find contours
        _contours.clear();
        findContours(outSegmented, _contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

        /// Draw outer contours
        Mat outContour = Mat::zeros( outSegmented.size(), CV_8UC3 );
        Scalar color = CV_RGB( 0, 255, 0 );

        size_t largestSize = 0;
        for( size_t i = 0; i < _contours.size(); i++ )
        {
            if(largestSize < _contours[i].size()) {
                largestSize = _contours[i].size();
                _largestContour = i;
            }
        }

        drawContours( outContour, _contours, _largestContour, color, 1, 8);

        if(showProcess)
        {
            /// Show result
            imshow( PROC_WINDOW, outSegmented );
            imshow( OUT_WINDOW, outContour );
            ///waitKey();
        }

        return true;
    }

public :
    FeatureGenerator() {
        namedWindow( PROC_WINDOW, WINDOW_AUTOSIZE );
        namedWindow( OUT_WINDOW, WINDOW_AUTOSIZE );
        _largestContour = 0;
    }

    ~FeatureGenerator() {}

    bool SetSource(const char* path) {
        /// Load source image
        _srcColor = imread(path, IMREAD_COLOR);
        if (_srcColor.empty())
        {
            cerr << NAME << __FUNCTION__ << "fail to file read" << endl;
            return false;
        }

        return true;
    };

    bool SetSource(Mat& src) {
        if (src.empty())
        {
            cerr << NAME << __FUNCTION__ << " input source is invalid" << endl;
            return false;
        }

        src.copyTo(_srcColor);

        return true;
    };

    void GetFourierDescriptors(vector< float >& descriptors, bool showProcess)
    {
        extractContour(showProcess);

        vector<Point>& contour = _contours[_largestContour];

        vector<float> ax, ay, bx, by;
        int numContourPoints = contour.size();

        for(int k = 0; k < NUM_CE; k++) {
            ax.push_back(0.0f); ay.push_back(0.0f);
            bx.push_back(0.0f); by.push_back(0.0f);

            float theta = 0.f;
            for(int u = 0; u < numContourPoints; u++) {
                theta = (2 * CV_PI * u * (k + 1)) / numContourPoints;

                ax[k] = ax[k] + contour[u].x * cos(theta);
                bx[k] = bx[k] + contour[u].x * sin(theta);
                ay[k] = ay[k] + contour[u].y * cos(theta);
                by[k] = by[k] + contour[u].y * sin(theta);
            }

            ax[k] = ax[k] / numContourPoints; ay[k] = ay[k] / numContourPoints;
            bx[k] = bx[k] / numContourPoints; by[k] = by[k] / numContourPoints;
        }

        descriptors.clear();
        descriptors.resize(NUM_CE);
        for(int k = 0; k < NUM_CE; k++) {
            descriptors[k] =
                sqrt((ax[k]*ax[k] + ay[k]*ay[k]) / (ax[0]*ax[0] + ay[0]*ay[0]))  +
                sqrt((bx[k]*bx[k] + by[k]*by[k]) / (bx[0]*bx[0] + by[0]*by[0]));

            printf("[FD(%d)] %.3f \t from ax:%.3f, ay:%.3f, bx:%.3f, by:%.3f \n",
                   k, descriptors[k], ax[k], ay[k], bx[k], by[k]);
        }
    }

};

}

#endif // FEATUREGENERATOR_H_INCLUDED
